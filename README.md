# gitlab-time-tracker

A command which help you with GitLab issues time tracking.

## Config

Install `curl` and `jq`.

Create a config file in your home `.config/gitlab-time-tracker/config.json`. Or run the program, and it will be create for you.

Fill the config.json `privateToken` value with your GitLab private API key. Add your project aliases inside the `projects` object, the format is: `"yourAlias": "theProjectId"`.

## Usage

### Start the timer for an issue
`gitlab-time-tracker.sh start projectAlias issueNumber`

___Notice:___ _the script not starting a real timer, only put the starting time into a status file, which found in the config directory. Never delete this file, except if you know what you're doing._

### Stop the work
`gitlab-time-tracker.sh stop` _or you can start another issue, and the previous will be stopped automatically_

___Notice:___ _the time only sended to GitLab when you stopped the timer, in some case it is not happening (like network error), you need to add manually based on the log file, which found in the config directory._

### Status line (for text based bars, like Polybar)
`gitlab-time-tracker.sh status`


