#!/usr/bin/env bash

configDir="$HOME/.config/gitlab-time-tracker/";
configFile="$HOME/.config/gitlab-time-tracker/config.json";
statusFile="$HOME/.config/gitlab-time-tracker/current.status";
logFile="$HOME/.config/gitlab-time-tracker/history.log";

help() {
    case "$1" in
    "start")
        echo "Starting a time for an issue.";
        echo "example: gitlab-time-tracker.sh start projectAlias issueId";
    ;;
    "stop")
        echo "Stopping the current timer and send the value to Gitlab.";
        echo "example: gitlab-time-tracker.sh stop";
    ;;
    "status")
        echo "Display the current working time.";
        echo "example: gitlab-time-tracker.sh status";
    ;;
    "")
        echo "Valid argument are:";
        echo "help, start, stop, status";
        echo "example: gitlab-time-tracker.sh help start";
    ;;
    esac;
}

[ $# -eq 0 ] && help && exit 0;

configCreate() {
    [ ! -f "$configDir" ] && mkdir "$configDir";
    [ ! -f "$configFile" ] && touch "$configFile";
}

[ ! -f "$configFile" ] && configCreate && echo "Config created, please fill first to continue!" && exit 0;

function config() {
    key="";
    for item in "$@"; do
        # the extra \"-s need for projects which has dash in the name
        key+=".\"$item\"";
    done;
    jq -r "$key" "$configFile"
}

function start() {
    # if already have a status file, we need to stop first
    [ -f "$statusFile" ] && ! stop && echo "Stop error, can't do anything!" && return 1;

    token=$(config privateToken);
    projectId=$(config projects $1);
    issueId=$2;
    [ -z "$token" ] && echo "Token variable is required, please fill the config file correctly!" && return 1;
    [ -z "$projectId" ] && echo "Project alias not found, please fill the config file correctly!" && return 1;
    [ -z "$issueId" ] && echo "Issue id not found!" && return 1;
    echo "$1 $2 $(date +%s)" > "$statusFile";
    echo "$1 project issue #$2 is started!";
    echo "$(date "+%Y-%m-%d %H:%M:%S") $1 project issue #$2 is started!" >> "$logFile";
    echo "$1 $2 $(date +%s)" >> "$logFile";
    return 0;
}

function stop() {
    # if no status file, do nothing
    [ ! -f "$statusFile" ] && echo "Nothing to stop!" && return 0;
    
    # read the status file data
    statusLine="$(<$statusFile)";
    token=$(config privateToken);
    [ -z "$token" ] && echo "Token variable is required, please fill the config file correctly!" && return 1;
    alias=$(cut -d' ' -f1 <<< "$statusLine");
    [ -z "$alias" ] && echo "Project alias not found! Maybe the status file is broken?" && return 1;
    projectId=$(config projects "$alias");
    [ -z "$projectId" ] && echo "Project alias not found, please fill the config file correctly!" && return 1;
    issueId=$(cut -d' ' -f2 <<< "$statusLine");
    [ -z "$issueId" ] && echo "Issue id not found! Maybe the status file is broken?" && return 1;
    timeStarted=$(cut -d' ' -f3 <<< "$statusLine");
    [ -z "$timeStarted" ] && echo "Starting time not found! Maybe the status file is broken?" && return 1;
    now=$(date +%s);
    spentSeconds="$((now-timeStarted))";
    spentTime=$(convertSecondsToReadable $spentSeconds);

    # send to gitlab
    result=$(curl --silent --request POST --header "PRIVATE-TOKEN: $token" "https://gitlab.com/api/v4/projects/$projectId/issues/$issueId/add_spent_time?duration=$spentTime");
    returnedTime=$(jq ".human_total_time_spent // empty" <<< ${result});
    [ -z "$returnedTime" ] && echo "Failed to send the spend time!" && echo "The result was: $result" && return 1;

    # ok, the request was success
    echo "$projectId project issue #$issueId is stopped!";
    echo "The issue time spent changed to: $returnedTime";

    # remove the status file
    rm "$statusFile" && echo "Status file removed!";
    echo "$(date "+%Y-%m-%d %H:%M:%S") $projectId project issue #$issueId is stopped!" >> "$logFile";

    return 0;
}

function convertSecondsToReadable() {
    seconds="$1";
    [ "$seconds" -gt 60 ] && minutes="$(($seconds/60))" && seconds="$(($seconds-60*$minutes))";
    [ -z "$minutes" ] && echo "${seconds}s" && return 0;
    [ "$minutes" -gt 60 ] && hours="$(($minutes/60))" && minutes="$(($minutes-60*$hours))";
    [ -z "$hours" ] && echo "${minutes}m${seconds}s" && return 0;
    echo "${hours}h${minutes}m";
    return 0;
}

function status() {
    [ ! -f "$statusFile" ] && echo "Not running!" && return 0;
    statusLine="$(<$statusFile)";
    now=$(date +%s);
    timeStarted=$(cut -d' ' -f3 <<< "$statusLine");
    [ -z "$timeStarted" ] && echo "Starting time not found! Maybe the status file is broken?" && return 1;
    spentSeconds="$((now-timeStarted))";
    spentTime=$(convertSecondsToReadable $spentSeconds);
    alias=$(cut -d' ' -f1 <<< "$statusLine");
    [ -z "$alias" ] && echo "Project alias not found! Maybe the status file is broken?" && return 1;
    issueId=$(cut -d' ' -f2 <<< "$statusLine");
    [ -z "$issueId" ] && echo "Issue id not found! Maybe the status file is broken?" && return 1;
    echo "$alias #$issueId $spentTime";
    return 0;
}

case "$1" in
"help")
    [ $# -eq 2 ] && help $2 && exit 0;
    help;
    exit 1;
    ;;
"start")
    [ $# -eq 3 ] && start $2 $3 && exit 0;
    exit 1;
    ;;
"status")
    status && exit 0;
    exit 1;
    ;;
"stop")
    [ $# -eq 1 ] && stop && exit 0;
    exit 1;
    ;;
"")
    help;
    ;;
esac
